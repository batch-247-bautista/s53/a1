import React from 'react';
import { Link } from 'react-router-dom';
import Banner from './Banner';

export default function ErrorPage() {
  return (
    <div>
      <Banner text="Oops! Something went wrong." />
      <p>Page not found. Go back to the <Link to="/">homepage</Link>.</p>
    </div>
  );
}
